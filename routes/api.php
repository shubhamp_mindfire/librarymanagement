<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login', "AuthController@login");
Route::post('/register', "AuthController@register");

Route::get('/users', "LibraryUserController@getusers");
Route::get('/alllibraryusers', "LibraryUserController@alllibraryusers");
Route::post('/users', "LibraryUserController@store");
Route::get('/users/{id}', "LibraryUserController@getUserdetail");
Route::put('/users/{id}', "LibraryUserController@update");
Route::delete('/users/{id}', "LibraryUserController@destroy");
Route::post('/users/multipledelete', "LibraryUserController@multipleDelete");

Route::get('/books', "LibraryBookController@getbooks");
Route::post('/books', "LibraryBookController@storebook");
Route::get('/books/{id}', "LibraryBookController@getBookdetail");
Route::put('/books/{id}', "LibraryBookController@updateBook");
Route::delete('/books/{id}', "LibraryBookController@destroyBook");
Route::post('/books/multipledelete', "LibraryBookController@multipleBookDelete");


Route::get('/getissuedbooks/{id}', "LibraryController@getissuedBooksByUser");
Route::get('/getallissuedbooks', "LibraryController@getallissuedbooks");
Route::get('/getbooksforissue', "LibraryController@getbooksforissue");
Route::post('/issue-book/{book_id}/{user_id}', "LibraryController@issueBook");
Route::post('/return-book/{transaction_id}', "LibraryController@returnBook");
Route::put('/update-duedate/{transaction_id}', "LibraryController@updateDuedate");

Route::middleware('auth:api')->post('/logout', "AuthController@logout");

