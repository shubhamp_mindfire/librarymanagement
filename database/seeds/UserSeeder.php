<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Book;
use App\Models\LibraryUser;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>"Demo Admin",
            'email'=>"demo@demo.com",
            'password'=>bcrypt("demo@123")
        ]);
        for($i=1;$i<10;$i++){
            Book::create([
                'title'=>"Book ".$i,
                'isbn'=>$i*33,
                'shelf_no'=>$i*2,
                'stock'=>100
            ]);
        }
        for($i=1;$i<10;$i++){
            LibraryUser::create([
                'name'=>'User '.$i,
                'gender'=>'male',
                'email'=>'user'.$i.'@gmail.com',
                'primary_phone'=>'9999999999',
                'address_line1'=>'123-43'.$i,
                'address_line2'=>null,
                'city'=>'Delhi',
                'street'=>'New Delhi',
                'zip'=>'11000'.$i,
                'country'=>'India'
            ]);
        }
    }
}
