window.Vue = require('vue');
// Vue.prototype.$auth_id = document.querySelector("meta[name='auth_id']").getAttribute('content');
// Vue.prototype.$auth_name = document.querySelector("meta[name='auth_name']").getAttribute('content');
// Vue.prototype.$auth_email = document.querySelector("meta[name='auth_email']").getAttribute('content');
Vue.prototype.$auth_id = 1
Vue.prototype.$auth_name = "shubham"
Vue.prototype.$auth_email = "shubham@gmail.com"

// Client ID and API key from the Developer Console
const CLIENT_ID = '890974279430-g8ej6oohqnq885do9tc5g8a1f80uue5e.apps.googleusercontent.com';
const API_KEY = 'AIzaSyCyhVqAO1GBlRqnYo4InS0WXn3pqakAGVo';
// Array of API discovery doc URLs for APIs used by the quickstart
const DISCOVERY_DOCS = ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'];
// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
// const SCOPES = 'https://www.googleapis.com/auth/calendar';
const SCOPES = 'https://www.googleapis.com/auth/calendar.events';
Vue.mixin({
    methods: {
        
        // Load index=====================
        loadIndex(page = 1) {
            this.$Progress.start();
            axios.get('/api' + this.dataSet.path + '?page='+page)
                .then(({
                    data
                }) => {
                    // this.fetchedRows = data
                    this.fetchedRows = data.data //For pagination
                    this.dataSet.loading = false;
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        new Swal({
                            type: 'error',
                            title: 'Oops... Something went wrong',
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            // footer: '<a href>Report this issue</a>'
                        })
                    }
                });
        },

        getResults(page = 1) {
            axios.get('/api'+this.dataSet.path+'?page=' + page)
                .then(response => {
                    this.fetchedRows = response.data;
                });
        },
        
        
        // Custom store methods==================
        customStore(defaultClass = ".ui.form", path = this.dataSet.path) {
            if (true) {
                this.$Progress.start();
                this.form.post('/api' + path)
                    .then(() => {
                        toast.fire({
                            type: 'success',
                            title: this.dataSet.toastMsg,
                        })
                        this.$Progress.finish();
                        this.$router.push(this.dataSet.path);
                    })
                    .catch((error) => {
                        console.log(error)
                        this.$Progress.fail();
                        if (error.status == 401) {
                            this.$router.push('/loginf');
                        } else {
                            if (error.status == 422) {
                                 Swal({
                                    type: 'error',
                                    title: 'Validation error',
                                    text: 'Please review the validation error messages in the fields highlighted in red',
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                })
                            }
                            else {
                                new Swal({
                                    type: 'error',
                                    title: 'Oops... Something went wrong',
                                    text: error,
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                    // footer: '<a href>Report this issue</a>'
                                })
                            }
                        }
                    });
            }
        },


        // Delete entry====================

        customDelete(id) {

            new Swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    this.form.delete('/api' + this.dataSet.path + '/' + id)
                        .then((data) => {
                            if(data.data[1]){
                                toast.fire({
                                    type: 'success',
                                    title: this.dataSet.deleteMsg,
                                })
                                this.$router.push(this.dataSet.path);
                            }
                            else{
                                new Swal({
                                    type: 'error',
                                    title: 'Oops... Something went wrong',
                                    text: data.data[0],
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                    // footer: '<a href>Report this issue</a>'
                                })
                            }
                        })
                        .catch((error) => {
                            if (error.response.status == 401) {
                                this.$router.push('/loginf');
                            } else {
                                new Swal({
                                    type: 'error',
                                    title: 'Oops... Something went wrong',
                                    text: error,
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                    // footer: '<a href>Report this issue</a>'
                                })
                            }
                        });
                }

            })

        },

        // Update entry====================

        customUpdate() {

            this.$Progress.start();
            this.form.put('/api' + this.dataSet.path + '/' + this.profileData.id)
                .then(() => {
                    Fire.$emit('loadprofile')
                    
                    toast.fire({
                        type: 'success',
                        title: this.dataSet.toastMsg,
                    })
                    this.$Progress.finish();
                    this.$router.push(this.dataSet.path);
                })
                .catch((error) => {
                    this.$Progress.fail();
                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        if (error.response.status == 422) {
                            new Swal({
                                type: 'error',
                                title: 'Validation error',
                                text: 'Please review the validation error messages in the fields highlighted in red',
                                showCloseButton: true,
                                showConfirmButton: false,
                            })
                        }
                        else {
                            new Swal({
                                type: 'error',
                                title: 'Oops... Something went wrong',
                                text: error,
                                showCloseButton: true,
                                showConfirmButton: false,
                                // footer: '<a href>Report this issue</a>'
                            })
                        }
                    }

                });

        },


        // Load single entry==========================
        
        loadProfile(id) {
            this.$Progress.start();
            axios.get('/api' + this.dataSet.path + '/' + id)
                .then(({
                    data
                }) => {
                    this.profileData = data;
                    this.form.fill(data);
                    this.dataSet.loading = false;
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        new Swal({
                            type: 'error',
                            title: 'Oops... Something went wrong',
                            text: error.response.data,
                            showCloseButton: true,
                            showConfirmButton: false,
                            // footer: '<a href>Report this issue</a>'
                        })
                    }
                });
        },

        viewEditForm(id) {
            this.$router.push(this.dataSet.path + '/' + id + '/edit');
        },

        // Delete list items=====================
        deleteSelected() {

            new Swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    this.$Progress.start();
                    const checkboxids = this.dataSet.eachCheckbox;
                    const url = '/api' + this.dataSet.path + '/multipledelete';

                    axios.post(url, {
                            ids: checkboxids
                        })
                        .then(({
                            data
                        }) => {
                            Fire.$emit('loadindex')
                            if (data[2]){
                                toast.fire({
                                    type: 'success',
                                    title: data[0],
                                })
                            }
                            else{
                                var items='';
                                data[1].forEach(element => {
                                    items = items+"<li>"+element+"</li>"
                                });
                                items="<ul class='swal_ul'>"+items+"</ul>";
                                new Swal({
                                    title: '<strong><u>' + data[0]+'</u></strong>',
                                    type: 'warning',
                                    html:
                                        'The following entries could not be deleted because there are non returned books: <br>' +
                                        items,
                                    showCloseButton: true,
                                    showCancelButton: false,
                                    focusConfirm: true,
                                    cancelButtonText:
                                        'Ok',
                                })
                            }
                            // this.$Progress.finish();
                        })
                        .catch((error) => {
                            this.$Progress.fail();
                            if (error.response.status == 401) {
                                this.$router.push('/login');
                            } else {
                                new Swal({
                                    type: 'error',
                                    title: 'Oops... Something went wrong',
                                    text: error,
                                    showCloseButton: true,
                                    showConfirmButton: false,
                                    // footer: '<a href>Report this issue</a>'
                                })
                            }
                        });
                }

            })
        },

        toggleCheckbox() {
            this.selectAll = !this.selectAll
            if (this.selectAll) {
                this.fetchedRows.forEach((fetchedRow, index) => {
                    Vue.set(this.dataSet.eachCheckbox, index, fetchedRow.id)
                })
            } else {
                this.dataSet.eachCheckbox = []
            }
        },

        // Fetch - helper methods==============================
        allLibraryUsers(link){
            this.$Progress.start();
            axios.get('/api' + link)
                .then(({
                    data
                }) => {
                    this.libraryusers = data
                        // console.log(isDropdown)
                        this.libraryusers.unshift({
                            key:'',
                            value:'',
                            name:'Select'
                        });
                        // console.log(data);

                    
                    this.$Progress.finish();
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        new Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },
        getAllIssuedBooks(){
            this.$Progress.start();
            axios.get('/api' +'/getallissuedbooks')
                .then(({
                    data
                }) => {
                    this.issuedbooks = data
                    
                    this.$Progress.finish();
                    this.dataSet.loading=false
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },
        getIssuedBooks(id){
            this.$Progress.start();
            axios.get('/api' +'/getissuedbooks/'+id)
                .then(({
                    data
                }) => {
                    this.issuedbooks = data
                    
                    this.$Progress.finish();
                    this.dataSet.loading=false
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },
        getBooksForIssue(){
            this.$Progress.start();
            axios.get('/api' +'/getbooksforissue')
                .then(({
                    data
                }) => {
                    this.books = data
                    
                    this.$Progress.finish();
                    this.dataSet.loading=false
                })
                .catch((error) => {
                    this.$Progress.fail();
                    // this[errorStoringVar] = error;

                    if (error.response.status == 401) {
                        this.$router.push('/loginf');
                    } else {
                        Swal({
                            type: 'error',
                            title: swalTitle,
                            text: error,
                            showCloseButton: true,
                            showConfirmButton: false,
                            footer: '<a href=' + issueReportinglink + '>Report this issue</a>'
                        })
                    }
                });
        },
        


        customFormatter(date) {
            return this.$options.filters.customFormatter(date);
        },
        changeDateFormat(date, tz,format) {
            return this.$options.filters.myDate(date, tz , format);
        },

        phpDate() {
            x = new Date();
            return this.$options.filters.myDateInISOString(x);
        },
        
        getDefaultDueDate() {
            var myCurrentDate=new Date();
            var myFutureDate=new Date(myCurrentDate);
                myFutureDate.setDate(myFutureDate.getDate()+ 8);
            return myFutureDate;
        },
        pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        },
        searchQuery(query) {
            this.$Progress.start();
            axios.get('/api' + this.dataSet.path + '/find?q=' + query)
                .then((data) => {
                    this.fetchedRows = data.data;
                    this.$Progress.finish();
                })
                .catch((error) => {
                    console.log(error)
                    this.$Progress.fail();
                })
        },
        getColor(color){
            switch (color) {
                case "Open":
                    return "yellow"
                    break;
                case "Lost":
                    return "grey"
                    break;

                default:
                    break;
            }
        },

    //    Google api start

    /**
     *  On load, called to load the auth2 library and API client library.
     */
    handleClientLoad() {
        gapi.load('client:auth2', this.initClient);
      },
  
      /**
       *  Initializes the API client library and sets up sign-in state
       *  listeners.
       */
      initClient() {
        let vm = this;
  
        gapi.client.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,
          scope: SCOPES
        }).then(_ => {
          // Listen for sign-in state changes.
          // vm.api.auth2.getAuthInstance().isSignedIn.listen(vm.authorized);
          gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus);
  
            // Handle the initial sign-in state.
            this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            // this.getData()
            
            
        });
      },
      updateSigninStatus(isSignedIn) {
          if (isSignedIn) {
            // this.authorized = true;
            this.$store.dispatch("retrieveGoogleAuthState",true)
          } else {
            // this.authorized = false;
            this.$store.dispatch("retrieveGoogleAuthState",false)
          }
        },
    
      /**
       *  Sign in the user upon button click.
       */
      handleAuthClick(event) {
        Promise.resolve(gapi.auth2.getAuthInstance().signIn())
          .then(res => {
            // this.authorized = true;
            this.$store.dispatch("retrieveGoogleAuthState",true)
            // console.log(res.wc);
            // localStorage.setItem("google_access", res.wc)
          });
      },
      
      /**
       *  Sign out the user upon button click.
       */
      handleSignoutClick(event) {
        Promise.resolve(gapi.auth2.getAuthInstance().signOut())
          .then(_ => {
            // this.authorized = false;
            this.$store.dispatch("retrieveGoogleAuthState",false)
          });
      },
    
    //    Google api end

    googleAuthStatus(){
        // true
        return this.$store.getters.googleAuth
      }

    },
    computed: {
        loggedIn(){
            // true
            return this.$store.getters.loggedIn
          },
    },
    mounted() {
        if(!this.loggedIn){
            if(this.$route.path!='/login'){
                this.$router.push("/login");
            }
        }
    },

    data: function () {
        return {
            // authorized: false,
            showClearFilter: false,
        }
    },

    watch: {
        'dataSet.eachCheckbox': function (values) {
            // console.log(this.fetchedRows);
            if (values != undefined && this.fetchedRows != undefined) {
                if (values.length === this.fetchedRows.length) {
                    this.selectAll = true
                } else {
                    this.selectAll = false
                }
            }
        },
    },

    

})
