/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./helpers.js');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import VueRouter from 'vue-router'
import { Form, HasError, AlertError } from 'vform'
import VueProgressBar from 'vue-progressbar'
import Swal from 'sweetalert2'
import routes from './router'
import {store} from './store/store'

var moment = require('moment-timezone');

Vue.use(VueRouter)
// form
window.Form=Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const baroptions = {
    color: '#fad03b',
    failedColor: '#ff7500',
    thickness: '5px',
    transition: {
      speed: '0.2s',
      opacity: '0.6s',
      termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
  }
  Vue.use(VueProgressBar, baroptions)


  Vue.filter('myDate',function(created,timezone,format){
    if (moment(created).isValid()) {
      return moment(created).tz(timezone).format(format)
      // return moment(created).format(format)
    }
    else {
      // return "-";
    }
})

Vue.filter('myDateInISOString', function (created) {
  if (moment(created).isValid()) {
    return moment(created).toISOString()
  }
  else {
  }
})

Vue.filter('myDateForHumans', function (created, timezone) {
  if (moment(created).isValid()) {
    return moment(created).tz(timezone).add(5,'hours').add(30,'minutes').fromNow()
  }
  else {
    // return "-";
  }
})

Vue.filter('customFormatter', function (datetime) {
  return moment(datetime).format('YYYY-MM-DD HH:mm:00');
})


Vue.filter('isBeforeNow',function(deadline){
  return moment(deadline).isBefore(moment().toISOString())
})
Vue.filter('isAfter7Days',function(deadline){
  // var dateafterweek = moment(deadline).add(7, 'days')
  var deadline1 = moment(deadline)
  var currentdate = moment(new Date())

  if (deadline1.diff(currentdate, 'days')>=7){
    return true;
  }
})
// moment filters
// arithmatic filters
Vue.filter('roundupto2',function(y){
    return Math.round(y * 100 + Number.EPSILON) / 100
  })
  Vue.filter('zeroPad', function zeroPad(n, str , length=6){
    var s = n + "", needed=length - s.length;
    if(needed> 0) s = (Math.pow(10, needed) + "").slice(1) + s;
    return str+s;
  })
  
  var numeral = require("numeral");
  Vue.filter("formatNumber6", function (value) {
    return numeral(value).format("000000");
  });
  
// arithmatic filters

// swal config
window.Swal=Swal;

const toast = Swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 3000
});

window.toast=toast;
window.Fire = new Vue;
// swal config

// routers
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})
// routers

const app = new Vue({
    el: '#app',
    router,
    store
});
