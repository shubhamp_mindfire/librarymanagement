// import Vue from 
import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';

Vue.use(Vuex)

export const store= new Vuex.Store({
    state:{
        token:localStorage.getItem("access_token") || null,
        loggedInuser:{name:null},
        authorized:false,
    
    },
    getters:{
        loggedIn(state){
            return state.token !== null
        },
        googleAuth(state){
            return state.authorized
        }
    },
    mutations:{
        retrieveToken(state,token){
            state.token=token
        },
        retrieveGoogleAuthState(state,AuthState){
            state.authorized=AuthState
        },
        destroyToken(state){
            state.token=null
            state.loggedInuser={name:null}
        },
        retrieveUserDetail(state,userdetail){
            state.loggedInuser=userdetail
        }
    },
    actions:{
        retrieveToken(context, credentials) {
            return new Promise((resolve,reject) => {
                Axios.post('/api/login', {
                    username: credentials.username,
                    password: credentials.password
                    })
                    .then(response=>{
                        const token= response.data.access_token
                        // console.log(response)
                        localStorage.setItem("access_token", token)
                        context.commit('retrieveToken',token)
                        resolve(response)
                    })
                    .catch(err => {
                        // console.log(err)
                        new Swal({
                            type: 'error',
                            title: 'Oops... Something went wrong',
                            text: err.response.data,
                            showCloseButton: true,
                            showConfirmButton: false,
                            // footer: '<a href>Report this issue</a>'
                        })
                        reject(err)
                    })
                
            })
        },
        retrieveUserDetail(context){
            Axios.defaults.headers.common['Authorization']='Bearer '+context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) => {
                    Axios.get('/api/user')
                        .then(response=>{
                            const userdetails= response.data
                            context.commit('retrieveUserDetail',userdetails)
                            resolve(response)
                        })
                        .catch(err => {
                            console.log(err)
                            reject(err)
                        })
                    
                })
            }

        },
        destroyToken(context){
            Axios.defaults.headers.common['Authorization']='Bearer '+context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) => {
                    Axios.post('/api/logout')
                        .then(response=>{
                            localStorage.removeItem("access_token")
                            context.commit('destroyToken')
                            resolve(response)
                        })
                        .catch(err => {
                            localStorage.removeItem("access_token")
                            context.commit('destroyToken')
                            reject(err)
                        })
                    
                })
            }
        },
        retrieveGoogleAuthState(context, authState){
            // Axios.defaults.headers.common['Authorization']='Bearer '+context.state.token
            const authSet= authState
            context.commit('retrieveGoogleAuthState',authSet)
        }
    }
})
