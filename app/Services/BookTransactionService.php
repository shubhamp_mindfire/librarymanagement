<?php

namespace App\Services;

use App\Models\BookTransaction;

class BookTransactionService
{

    public function getAllIssuedLibraryBooks()
    {
        return BookTransaction::with(['bookdetail', 'userdetail'])->orderBy('updated_at', 'desc')->get();
    }

    public function getIssuedBooksByUser($id)
    {
        return BookTransaction::with(['bookdetail', 'userdetail'])->where('library_user_id', $id)->whereNull('return_date')->get();
    }
    public function issueBook($due_date,$book_id, $user_id)
    {
        $book_transaction_id=BookTransaction::create([
            'library_user_id' => $user_id,
            'book_id' => $book_id,
            'issue_date' => date('Y-m-d'),
            'due_date' => $due_date,
        ])->id;

        return $book_transaction_id;
        
    }

    // to return a book
    public function returnBook($transaction_id)
    {
        BookTransaction::whereId($transaction_id)->update([
            'return_date' => date('Y-m-d'),
        ]);
    }
    // to return a book
    public function updateDuedate($due_date,$transaction_id)
    {
        BookTransaction::whereId($transaction_id)->update([
            'due_date' => $due_date,
        ]);
    }

}
