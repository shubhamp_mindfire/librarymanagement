<?php

namespace App\Services;

use App\Models\LibraryUser;

class LibraryUserService
{

    public function getAllLibraryUsers()
    {
        return LibraryUser::query()->orderBy('name')->get();
    }
    public function getAllLibraryUserspagination()
    {
        return LibraryUser::query()->orderBy('name')->paginate(100);
    }
    public function storeLibraryUser($request)
    {
        LibraryUser::create([
            'name' => $request->name,
            'gender' => $request->gender,
            'email' => $request->email,
            'primary_phone' => $request->primary_phone,
            'secondary_phone' => $request->secondary_phone,
            'address_line1' => $request->address_line1,
            'address_line2' => $request->address_line2,
            'city' => $request->city,
            'street' => $request->street,
            'zip' => $request->zip,
            'country' => $request->country,
        ]);
    }
    public function updateLibraryUser($id, $request)
    {
        LibraryUser::whereId($id)->update([
            'name' => $request->name,
            'gender' => $request->gender,
            'email' => $request->email,
            'primary_phone' => $request->primary_phone,
            'secondary_phone' => $request->secondary_phone,
            'address_line1' => $request->address_line1,
            'address_line2' => $request->address_line2,
            'city' => $request->city,
            'street' => $request->street,
            'zip' => $request->zip,
            'country' => $request->country,
        ]);
    }
    public function getLibraryUserdetail($id)
    {
        return LibraryUser::findorFail($id);
    }
    public function deleteSingleUser($id)
    {
        if (!LibraryUser::findorFail($id)->isAssociated()) {
            LibraryUser::findorFail($id)->delete();
            return ["User deleted", true];
        }
        return ['This user cannot be deleted because transactions for this user already exist', false];
    }
    public function deleteMultipleUsers($request)
    {

        $notdeletedid = [];
        foreach ($request->ids as $id) {
            if (!LibraryUser::findorFail($id)->isAssociated()) {
                LibraryUser::findorFail($id)->delete();
            } else {
                $notdeletedid[] = $id;
            }
        }
        if (count($notdeletedid) > 0) {
            $display_var = LibraryUser::whereIn('id', $notdeletedid)->select('name')->pluck('name');
            return ["User(s) could not be deleted", $display_var, false];
        } else {
            return ["User(s) deleted", '', true];
        }
    }

}
