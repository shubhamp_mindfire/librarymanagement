<?php

namespace App\Services;

use App\Models\Book;

class LibraryBookService
{

    public function getAllLibraryBooksPaginate()
    {
        return Book::query()->orderBy('title')->paginate(100);
    }
    public function getAllLibraryBooks()
    {
        return Book::query()->orderBy('title')->get();
    }
    public function storeLibraryBook($request)
    {
        Book::create([
            'title' => $request->title,
            'isbn' => $request->isbn,
            'shelf_no' => $request->shelf_no,
            'stock' => $request->stock,
        ]);
    }
    public function updateLibraryBook($id, $request)
    {
        Book::whereId($id)->update([
            'title' => $request->title,
            'isbn' => $request->isbn,
            'shelf_no' => $request->shelf_no,
            'stock' => $request->stock,
        ]);
    }
    public function getLibraryBookdetail($id)
    {
        return Book::findorFail($id);
    }
    public function deleteSingleBook($id)
    {
        if (!Book::findorFail($id)->isAssociated()) {
            Book::findorFail($id)->delete();
            return ["Book deleted", true];
        }
        return ['This book cannot be deleted because transactions for this book already exist', false];
    }
    public function deleteMultipleBooks($request)
    {

        $notdeletedid = [];
        foreach ($request->ids as $id) {
            if (!Book::findorFail($id)->isAssociated()) {
                Book::findorFail($id)->delete();
            } else {
                $notdeletedid[] = $id;
            }
        }
        if (count($notdeletedid) > 0) {
            $display_var = Book::whereIn('id', $notdeletedid)->select('title')->pluck('title');
            return ["Book(s) could not be deleted", $display_var, false];
        } else {
            return ["Book(s) deleted", '', true];
        }
    }

}
