<?php

namespace App\Http\Controllers;

use App\Services\BookTransactionService;
use App\Services\LibraryBookService;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function __construct(BookTransactionService $booktransactionservice)
    {
        // $this->middleware('auth:api');
        $this->booktransactionservice = $booktransactionservice;
    }

    // Issue return part
    // For book transaction page to see all the issued books
    public function getallissuedbooks()
    {
        try {
            return $this->booktransactionservice->getAllIssuedLibraryBooks();
        } catch (\Exception $e) {
            return response()->json("Database error. Please try again later", 500);
        }
    }

    // to get all the non returned books by a user
    public function getissuedBooksByUser($id)
    {
        try {
            return $this->booktransactionservice->getIssuedBooksByUser($id);
        } catch (\Exception $e) {
            return response()->json("Database error. Please try again later", 500);
        }
    }

    // to get all the books available
    public function getbooksforissue(LibraryBookService $librarybookservice)
    {
        try {
            return $librarybookservice->getAllLibraryBooks();
        } catch (\Exception $e) {
            return response()->json("Database error. Please try again later", 500);
        }
    }

    // To issue a book
    public function issueBook(Request $request,$book_id, $user_id)
    {
        try {
            $book_transaction_id=$this->booktransactionservice->issueBook($request->due_date,$book_id, $user_id);
            return response()->json(["Book issued",$book_transaction_id], 200);
        } catch (\Exception $e) {
            return response()->json("Book could not be issued. Please try again later", 500);
        }
    }

    // to return a book
    public function returnBook($transaction_id)
    {
        try {
            $this->booktransactionservice->returnBook($transaction_id);
            return response()->json("Book returned", 200);
        } catch (\Exception $e) {
            return response()->json("Book could not be returned. Please try again later", 500);
        }
    }
    // to extend the due date of issued book
    public function updateDuedate(Request $request, $transaction_id)
    {
        try {
            $this->booktransactionservice->updateDuedate($request->due_date, $transaction_id);
            return response()->json("Due date extended", 200);
        } catch (\Exception $e) {
            return response()->json("Due date could not be extended. Please try again later", 500);
        }
    }
}
