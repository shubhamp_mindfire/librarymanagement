<?php

namespace App\Http\Controllers;

use App\Http\Requests\LibraryUserStoreRequest;
use App\Http\Requests\LibraryUserUpdateRequest;
use App\Services\LibraryUserService;
use Illuminate\Http\Request;

class LibraryUserController extends Controller
{
    public function __construct(LibraryUserService $libraryuserservice)
    {
        $this->libraryuserservice = $libraryuserservice;
    }
    // For user page
    public function getusers()
    {
        try {
            return $this->libraryuserservice->getAllLibraryUserspagination();
        } catch (\Exception $e) {
            return response()->json("Database error. Please try again later", 500);
        }
    }

    // list of all users For Issue/Return page
    public function alllibraryusers()
    {
        try {
            return $this->libraryuserservice->getAllLibraryUsers();
        } catch (\Exception $e) {
            return response()->json("Database error. Please try again later", 500);
        }
    }
    // Store a user
    public function store(LibraryUserStoreRequest $request)
    {
        try {
            $this->libraryuserservice->storeLibraryUser($request);
            return response()->json('User details saved', 201);
        } catch (\Exception $e) {
            return response()->json("User details could not be saved. Please try again later", 500);
        }
    }
    // Update user details
    public function update($id, LibraryUserUpdateRequest $request)
    {
        try {
            $this->libraryuserservice->updateLibraryUser($id, $request);
            return response()->json('User details updated', 200);
        } catch (\Exception $e) {
            return response()->json("User details could not be updated. Please try again later", 500);
        }
    }

    // For edit page of user
    public function getUserdetail($id)
    {
        try {
            return $this->libraryuserservice->getLibraryUserdetail($id);
        } catch (\Exception $e) {
            return response()->json("User not found", 404);
        }
    }
    // Deleting a user
    public function destroy($id)
    {
        return $this->libraryuserservice->deleteSingleUser($id);
    }
    // Deleting multiple users
    public function multipleDelete(Request $request)
    {
        return $this->libraryuserservice->deleteMultipleUsers($request);
    }
}
