<?php

namespace App\Http\Controllers;

use App\Http\Requests\LibraryBookRequest;
use App\Services\LibraryBookService;
use Illuminate\Http\Request;

class LibraryBookController extends Controller
{
    public function __construct(LibraryBookService $librarybookservice)
    {
        $this->librarybookservice = $librarybookservice;
    }

    // for books page
    public function getbooks()
    {
        try {
            return $this->librarybookservice->getAllLibraryBooksPaginate();
        } catch (\Exception $e) {
            return response()->json("Database error. Please try again later", 500);
        }
    }
    // store a book
    public function storebook(LibraryBookRequest $request)
    {
        try {
            $this->librarybookservice->storeLibraryBook($request);
            return response()->json("Book details saved.", 201);
        } catch (\Exception $e) {
            return response()->json("Book details could not be saved. Please try again later", 500);
        }

    }
    // update a book
    public function updateBook($id, LibraryBookRequest $request)
    {
        try {
            $this->librarybookservice->updateLibraryBook($id, $request);
            return response()->json("Book Updated", 200);
        } catch (\Exception $e) {
            return response()->json("Book details could not be updated. Please try again later", 500);
        }
    }

    // for book edit page
    public function getBookdetail($id)
    {
        try {
            return $this->librarybookservice->getLibraryBookdetail($id);
        } catch (\Exception $e) {
            return response()->json("Book not found", 404);
        }
    }

    // Delete a book
    public function destroyBook($id)
    {
        return $this->librarybookservice->deleteSingleBook($id);
    }

    // Delete multile books
    public function multipleBookDelete(Request $request)
    {
        return $this->librarybookservice->deleteMultipleBooks($request);
    }
}
