<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LibraryUserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'gender' => 'required',
            'email' => 'required|email|unique:library_users,email,' . $this->id,
            'primary_phone' => 'required',
            'address_line1' => 'required',
            'city' => 'required',
            'street' => 'required',
            'zip' => 'required',
            'country' => 'required',
        ];
    }
    public function messages()
    {
        return [
            "gender.required" => "The gender field is required",
            "email.required" => "The currency field is required",
            "primary_phone.required" => "The Primary Phone field is required",
            "address_line1.required" => "The address line 1 field is required",
            "city.required" => "The city field is required",
            "street.required" => "The street field is required",
            "zip.required" => "The zip field is required",
            "country.required" => "The country field is required",
        ];
    }
}
