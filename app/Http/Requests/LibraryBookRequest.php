<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LibraryBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'isbn' => 'required',
            'stock' => 'required',
        ];
    }

    public function messages()
    {
        return [
            "title.required" => "The title field is required",
            "isbn.required" => "The ISBN field is required",
            "stock.required" => "The stock field is required",
        ];
    }
}
