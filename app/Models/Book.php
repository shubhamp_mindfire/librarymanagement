<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = [];

    public function isAssociated()
    {
        return BookTransaction::where('book_id', $this->id)->whereNull('return_date')->exists();
    }
    public function issuedBy()
    {
        return $this->belongsToMany('App\LibraryUser', 'book_transactions');
    }
}
